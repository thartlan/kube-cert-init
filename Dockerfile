FROM gcr.io/distroless/static

ADD kube-cert-init /kube-cert-init

CMD ["/kube-cert-init"]
