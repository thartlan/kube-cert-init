package main

import (
	"flag"
	"io/ioutil"
	"path"

	"k8s.io/klog"
)

var (
	serviceName      string
	serviceNamespace string

	outputPath string
)

func init() {
	flag.StringVar(&serviceName, "service-name", "", "name of the service to sign for")
	flag.StringVar(&serviceNamespace, "service-namespace", "", "namespace of the service to sign for")

	flag.StringVar(&outputPath, "output-path", "/certificates", "output directory to write .crt and .key files in")
}

func main() {
	klog.InitFlags(nil)
	flag.Set("logtostderr", "true")
	flag.Parse()

	klog.V(2).Info("Starting")

	clientset, err := getKubernetesClientset()
	if err != nil {
		klog.Fatalf("Could not create clientset: %v", err)
	}

	certPEM, keyPEM, err := getSignedCertificate(clientset)
	if err != nil {
		klog.Fatalf("Could not get signed certificate: %v", err)
	}

	crtFile := path.Join(outputPath, "server.crt")
	keyFile := path.Join(outputPath, "server.key")

	err = ioutil.WriteFile(crtFile, certPEM, 0644)
	if err != nil {
		klog.Fatalf("Could not write crt file: %v", err)
	}

	err = ioutil.WriteFile(keyFile, keyPEM, 0644)
	if err != nil {
		klog.Fatalf("Could not write key file: %v", err)
	}

	klog.V(2).Info("Wrote crt and key files")
	klog.V(2).Info("Done")
}
