package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"net"
	"os"
	"time"

	"github.com/pkg/errors"
	certificates "k8s.io/api/certificates/v1beta1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/util/cert"
	"k8s.io/client-go/util/certificate/csr"
	"k8s.io/client-go/util/keyutil"
	"k8s.io/klog"
)

func generateCertificates() ([]byte, []byte, interface{}, error) {
	serviceURL := fmt.Sprintf("%s.%s.svc", serviceName, serviceNamespace)

	ipAddrs := []net.IP{}
	ipEnv := os.Getenv("POD_IP")
	if len(ipEnv) > 0 {
		ip := net.ParseIP(ipEnv)
		ipAddrs = append(ipAddrs, ip)
	}

	template := x509.CertificateRequest{
		Subject: pkix.Name{
			CommonName: serviceURL,
		},
		IPAddresses: ipAddrs,
	}

	privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, nil, nil, errors.Wrap(err, "could not generate key")
	}

	der, err := x509.MarshalECPrivateKey(privateKey)
	if err != nil {
		return nil, nil, nil, errors.Wrap(err, "could not marshal key to DER")
	}

	keyPEM := pem.EncodeToMemory(&pem.Block{Type: keyutil.ECPrivateKeyBlockType, Bytes: der})

	csrPEM, err := cert.MakeCSRFromTemplate(privateKey, &template)
	if err != nil {
		return nil, nil, nil, errors.Wrap(err, "could not make csr from private key")
	}

	return csrPEM, keyPEM, privateKey, nil
}

func getSignedCertificate(clientset *kubernetes.Clientset) ([]byte, []byte, error) {
	klog.V(2).Info("Generating certificates")
	csrPEM, keyPEM, privateKey, err := generateCertificates()
	if err != nil {
		return nil, nil, errors.Wrap(err, "could not generate certificates")
	}

	certClient := clientset.CertificatesV1beta1().CertificateSigningRequests()

	usage := []certificates.KeyUsage{
		certificates.UsageDigitalSignature,
		certificates.UsageKeyEncipherment,
		certificates.UsageServerAuth,
	}

	klog.V(2).Info("Making certificate signing request")
	request, err := csr.RequestCertificate(certClient, csrPEM, "", usage, privateKey)
	if err != nil {
		return nil, nil, errors.Wrap(err, "could not create certificate signing request")
	}

	request.Status.Conditions = []certificates.CertificateSigningRequestCondition{
		{Type: certificates.CertificateApproved, Reason: "AutoApproved", Message: "Approved by auto-dns-init"},
	}

	klog.V(2).Info("Approving request")
	_, err = certClient.UpdateApproval(request)
	if err != nil {
		return nil, nil, errors.Wrap(err, "could not approve request")
	}

	klog.V(2).Info("Waiting for certificate to be issued")
	certData, err := csr.WaitForCertificate(certClient, request, time.Second*30)
	if err != nil {
		return nil, nil, errors.Wrap(err, "error waiting for certificate")
	}

	klog.V(2).Info("Certificate issued")

	return certData, keyPEM, nil
}
